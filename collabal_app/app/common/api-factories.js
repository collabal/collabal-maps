'use strict';
/**
 * Created by Asmita on 08 June 2015.
 * This is a collection of factories
 * Each factory uses $resource (or $http) for making a Api (REST/general) calls on the endpoint
 * -- Thoughts on using $http and $resource --
 * http://stackoverflow.com/questions/13181406/angularjs-http-and-resource
 * $http is for general purpose AJAX. With $http you're going to be making GET, POST, DELETE type calls manually
 * and processing the objects they return on your own.
 * $resource wraps $http for use in RESTful web API scenarios.
 * If you have an endpoint that deals with the same resource for more than one HTTP method,
 * then $resource is a good choice
 */

angular.module('SMSXChange')
	.factory('Login', function ($resource, appConfig) {
        var url = appConfig[appConfig.env].api_url + 'auth';

       // return $resource(url, { format: 'json', jsoncallback: 'JSON_CALLBACK' }, { 'load': { 'method': 'JSONP' } });
	  	return $resource(url);
})
/*
	.factory('filterList', function(){
	var filters = 
			{
				accountIDFilter : function(userAccountId)
					{
						return
						{	"name":"accountId",
							"op":"eq",
							"val": userAccountId
						}
					},
			 	dateFilter: function()
			 		{
						return
						{	"name":"accountId",
							"op":"eq",
							"val": "80001292"
						}
			 		}
			}
		return{
			getAccountIDFilter: function(userAccountId){			
				var q = {};
				q.filters = [];
				q.filters.push(filters.accountIDFilter(userAccountId));
				return '?q=' + JSON.stringify(q);
			}
});*/