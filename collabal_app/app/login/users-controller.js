angular.module('SMSXChange')
	.controller('LoginController', function($scope, $rootScope, $state, $http, $location, $localStorage, Login, appConfig) {
		$scope.territories = ["BITS Pilani, Goa", "BITS Pilani, Pilani Campus", "BITS Pilani, Hyd Campus", "IIT Delhi"];
		$rootScope.token = "";
		$localStorage.token = "";
		console.log("initial token from local storage ",$localStorage.token);
		var url = appConfig[appConfig.env].api_url + 'auth';
		 $scope.login = function() {
	       var params = {
				    "username": $scope.username,
				    "password": $scope.password
				};
				$http.post(url,params)
       			.success(function (response) {
       				$localStorage.token = response.token;
      	  			console.log(response);
      	  			console.log("token from local storage ",$localStorage.token);
      	  			$location.path('/templates');

    	 		}).error(function(data, status, headers, config) {
    	 			if(status == '400'){
    	 				$scope.loginError = true;	
    	 			}
  				});
  	 		
	        }
	        $rootScope.token = $localStorage.token;
 	}).controller('LogoutController', function($scope, $rootScope, $location, $localStorage) {
		$rootScope.token = "";
		$localStorage.token = "";
		console.log("logout token from local storage ",$localStorage.token);
		console.log("logout token from rootScope storage ",$rootScope.token);
      	$location.path('/login');
 	});

