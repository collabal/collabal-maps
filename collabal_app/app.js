/**
 * Collabal angular app
 * Version: 0.1
 */

/**
 * "use strict;" enables strict operating context. It prevents certain actions from being taken and throws more exceptions
 * With strict mode, you can not, for example, use undeclared variables.
 * http://stackoverflow.com/questions/1335851/what-does-use-strict-do-in-javascript-and-what-is-the-reasoning-behind-it
 */

"use strict";

/**
 * define an angular module called myApp
 * [...] - for dependency injection. module can depend on other modules
 * [Style Y023]
 * Only set once and get for all other instances.
 * https://github.com/johnpapa/angular-styleguide#style-y023
 */

angular
    .module('SMSXChange', [

        'ui.router', 'ngResource', 'ui.bootstrap',  'ngStorage', 'ngDraggable','ngSanitize', 'ngMap'

    ])
    .constant('appConfig', {

       /**
        * Set constant app level configuration here
        * For accessing these configurations in app, inject appConfig
        */
        env: 'development',
        development: {
            api_url: 'http://127.0.0.1:8000/api/v1/'
        },
        staging: {
             api_url: 'http://txtbox.in:5005/api/v1/'
        },
        production: {
            api_url: 'http://production.in:5005/api/v1/'
        }

    })
    .run(['$rootScope', '$state', '$stateParams', function ($rootScope,   $state,   $stateParams) {

        /**
         * It's very handy to add references to $state and $stateParams to the $rootScope
         * so that you can access them from any scope within your applications.For example,
         * <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
         * to active whenever 'contacts.list' or one of its decendents is active.
         */

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

    }])
    .config(function($stateProvider, $urlRouterProvider) {

        var viewBase = '/app/';

        $stateProvider
            .state('register', {
                url: '/register',
                data: {
                    pageTitle: 'Register'
                },
                views: {
                	'header': {
                        templateUrl: viewBase + 'partials/header.html'
                    },
                    'footer': {
                        templateUrl: viewBase + 'partials/footer.html'
                    },
                    'content': {
                        controller: 'RegistrationController',
                        templateUrl: viewBase + 'register/register.html'
                    }
                }
            })
            .state('login', {
                url: '/login',
                data: {
                    pageTitle: 'Login'
                },
                views: {
                	'header': {
                        templateUrl: viewBase + 'partials/header.html'
                    },
                    'content': {
                        controller: 'LoginController',
                        templateUrl: viewBase + 'login/login.html'
                    }
                }
            })
            .state('createTerritory', {
                url: '/createTerritory',
                data: {
                    pageTitle: 'CreateTerritory'
                },
                views: {
                    'header': {
                        templateUrl: viewBase + 'partials/header.html'
                    },
                    'content': {
                        controller: 'createTerritory',
                        templateUrl: viewBase + 'collabalMap/createTerritory/createTerritory.html'
                    }
                }
            })
            .state('pinUpdate', {
                url: '/pinUpdate',
                data: {
                    pageTitle: 'pinUpdate'
                },
                views: {
                    'header': {
                        templateUrl: viewBase + 'partials/header.html'
                    },
                    'content': {
                        controller: 'pinUpdate',
                        templateUrl: viewBase + 'collabalMap/pinUpdate/pinUpdate.html'
                    }
                }
            })
            .state('broadcastUpdate', {
                url: '/broadcastUpdate',
                data: {
                    pageTitle: 'broadcastUpdate'
                },
                views: {
                    'header': {
                        templateUrl: viewBase + 'partials/header.html'
                    },
                    'content': {
                        controller: 'pinUpdate',
                        templateUrl: viewBase + 'collabalMap/broadcastUpdate/broadcastUpdate.html'
                    }
                }
            })
            .state('mapLanding', {
                url: '/mapLanding',
                data: {
                    pageTitle: 'mapLanding'
                },
                views: {
                    'header': {
                        templateUrl: viewBase + 'partials/header.html'
                    },
                    'content': {
                        controller: 'mapLanding-controller',
                        templateUrl: viewBase + 'collabalMap/mapLanding/mapLanding.html'
                    }
                }
            })
        .state('logout', {
                url: '/logout',
                data: {
                    pageTitle: 'Login'
                },
                views: {
                    'content': {
                        controller: 'LogoutController',
                    }
                }
            })
      
        ;

        $urlRouterProvider.otherwise('/login');

    });

